## Build a CodeCommit git repo
module "codecommit" {
  source                         = "./codecommit"
  repository_name                = var.repository_name
  repository_deletion_protection = true
  default_tags                   = var.default_tags
  providers = {
    aws = aws
  }
}

## Build CodeBuild projects for Terraform Plan and Terraform Apply
module "codebuild" {
  source                                 = "./codebuild_tf"
  pipeline_name                          = var.pipeline_name
  terraform_plan_codebuild_project_name  = format("%s-TerraformPlan", title(var.pipeline_name))
  terraform_apply_codebuild_project_name = format("%s-TerraformApply", title(var.pipeline_name))
  //s3_logging_bucket_id                   = module.bootstrap.s3_logging_bucket_id
  codebuild_iam_role_arn = aws_iam_role.codebuild.arn
  //s3_logging_bucket                      = module.bootstrap.s3_logging_bucket
  providers = {
    aws = aws
  }
}

## Build a CodePipeline
module "codepipeline" {
  source                         = "./codepipeline"
  pipeline_name                  = var.pipeline_name
  terraform_codecommit_repo_name = module.codecommit.codecommit_repo_name
  codebuild_terraform_plan_name  = module.codebuild.terraform_plan_codebuild_project_name
  codebuild_terraform_apply_name = module.codebuild.terraform_apply_codebuild_project_name
  with_approval                  = var.with_approval
  branch_name                    = var.branch_name
  providers = {
    aws = aws
  }
}
