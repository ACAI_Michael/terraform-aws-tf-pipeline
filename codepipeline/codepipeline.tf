
resource "aws_codepipeline" "tf_codepipeline" {
  name     = format("%s_TerraformCodePipeline", var.pipeline_name)
  role_arn = aws_iam_role.tf_codepipeline_role.arn

  artifact_store {
    location = aws_s3_bucket.tf_codepipeline_artifact_bucket.bucket
    type     = "S3"
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeCommit"
      version          = "1"
      output_artifacts = [format("%s_SourceArtifacts", var.pipeline_name)]

      configuration = {
        RepositoryName = var.terraform_codecommit_repo_name
        BranchName     = var.branch_name
      }
    }
  }

  stage {
    name = "Terraform_Plan"

    action {
      name            = "Terraform-Plan"
      category        = "Build"
      owner           = "AWS"
      provider        = "CodeBuild"
      input_artifacts = [format("%s_SourceArtifacts", var.pipeline_name)]
      version         = "1"

      configuration = {
        ProjectName = var.codebuild_terraform_plan_name
      }
    }
  }

  dynamic "stage" {
    for_each = var.with_approval == true ? [1] : []
    content {
      name = "Manual_Approval"
      action {
        name     = "Manual-Approval"
        category = "Approval"
        owner    = "AWS"
        provider = "Manual"
        version  = "1"
      }
    }
  }

  stage {
    name = "Terraform_Apply"

    action {
      name            = "Terraform-Apply"
      category        = "Build"
      owner           = "AWS"
      provider        = "CodeBuild"
      input_artifacts = [format("%s_SourceArtifacts", var.pipeline_name)]
      version         = "1"

      configuration = {
        ProjectName = var.codebuild_terraform_apply_name
      }
    }
  }
}