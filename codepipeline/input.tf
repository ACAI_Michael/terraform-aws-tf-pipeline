variable "pipeline_name" {
  description = "Name of the TF CodePipeline S3 bucket for artifacts"
}

variable "terraform_codecommit_repo_name" {
  description = "Terraform CodeCommit repo name"
}

variable "codebuild_terraform_plan_name" {
  description = "Terraform plan codebuild project name"
}

variable "codebuild_terraform_apply_name" {
  description = "Terraform apply codebuild project name"
}

variable "with_approval" {
  type = bool
}

variable "branch_name" {
  type = string
}

