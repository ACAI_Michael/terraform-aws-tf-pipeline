# Build S3 bucket for CodePipeline artifact storage
resource "aws_s3_bucket" "tf_codepipeline_artifact_bucket" {
  bucket = format("%s-artifacts-bucket", lower(var.pipeline_name))
  acl    = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}
