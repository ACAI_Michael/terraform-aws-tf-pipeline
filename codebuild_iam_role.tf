# This role will be assumed by CodeBuild
resource "aws_iam_role" "codebuild" {
  name               = var.codebuild_role_name
  assume_role_policy = data.aws_iam_policy_document.codebuild_trust.json
  tags               = var.default_tags
}

data "aws_iam_policy_document" "codebuild_trust" {
  statement {
    sid    = "TrustPolicy"
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["codebuild.amazonaws.com"]
    }
    actions = [
      "sts:AssumeRole"
    ]
  }
}

resource "aws_iam_role_policy" "codebuild_target" {
  name   = replace(aws_iam_role.codebuild.name, "Role", "Policy")
  role   = aws_iam_role.codebuild.name
  policy = data.aws_iam_policy_document.codebuild_target.json
}

data "aws_iam_policy_document" "codebuild_target" {
  statement {
    sid    = "AllowListRoles"
    effect = "Allow"
    actions = [
      "iam:Get*",
      "iam:List*"
    ]
    resources = var.provisioning_target_role_arn
  }
  statement {
    sid    = "AllowAssumptionOfTargetRole"
    effect = "Allow"
    actions = [
      "sts:AssumeRole"
    ]
    resources = var.provisioning_target_role_arn
  }
}

resource "aws_iam_role_policy" "codebuild_internal" {
  name = format("%s_PolicyForCodeBuild", title(var.pipeline_name))
  role = aws_iam_role.codebuild.name

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Resource": [
        "*"
      ],
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:*"
      ],    
      "Resource": [
        "${module.codepipeline.tf_codepipeline_artifact_bucket_arn}",
        "${module.codepipeline.tf_codepipeline_artifact_bucket_arn}/*"
      ]
    },    
    {
      "Effect": "Allow",
      "Action": [
        "codecommit:BatchGet*",
        "codecommit:BatchDescribe*",
        "codecommit:Describe*",
        "codecommit:EvaluatePullRequestApprovalRules",
        "codecommit:Get*",
        "codecommit:List*",
        "codecommit:GitPull"
      ],
      "Resource": "${module.codecommit.codecommit_repo_arn}"
    },
    {
        "Effect": "Allow",
        "Action": [
            "ssm:DescribeParameters"
        ],
        "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
          "ssm:GetParameters"
      ],
      "Resource": [
          "arn:aws:ssm:eu-central-1:851519347965:parameter/codecommit_key",
          "arn:aws:ssm:eu-central-1:851519347965:parameter/codecommit_key_pass"
      ]
    }   
  ]
}
POLICY
}