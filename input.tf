variable "pipeline_name" {
  description = "CodeBuild git repository name"
  type        = string
}

variable "repository_name" {
  description = "A mapping of tags to assign to resources."
  type        = string
}
variable "codebuild_role_name" {
  description = ""
  type        = string
}

variable "provisioning_target_role_arn" {
  description = ""
  type        = list(string)
}

variable "with_approval" {
  type    = bool
  default = false
}

variable "branch_name" {
  type = string
  default = "main"
}


variable "default_tags" {
  type    = map(string)
  default = {}
}