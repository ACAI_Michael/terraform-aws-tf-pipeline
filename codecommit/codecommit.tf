resource "aws_codecommit_repository" "repo" {
  repository_name = var.repository_name
  description     = format("Terraform repo for %s", var.repository_name)
  tags = (var.repository_deletion_protection ?
    merge(
      {
        "CF:DeletionProtection" = "true"
      },
    var.default_tags) :
  var.default_tags)
}

