variable "repository_name" {
  description = "CodeBuild git repository name"
}

variable "repository_deletion_protection" {
  description = "sets a tag to protect the repo from deletion"
  type        = bool
}

variable "default_tags" {
  type    = map(string)
  default = {}
}