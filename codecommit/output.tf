
# Output the repo info back to main.tf
output "codecommit_repo_arn" {
  value = aws_codecommit_repository.repo.arn
}

output "codecommit_repo_name" {
  value = var.repository_name
}