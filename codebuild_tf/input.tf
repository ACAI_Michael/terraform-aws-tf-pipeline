variable "pipeline_name" {
  type = string
}

variable "terraform_plan_codebuild_project_name" {
  type = string
}

variable "terraform_apply_codebuild_project_name" {
  type = string
}

variable "codebuild_iam_role_arn" {
  type = string
}

variable "default_tags" {
  type    = map(string)
  default = {}
}