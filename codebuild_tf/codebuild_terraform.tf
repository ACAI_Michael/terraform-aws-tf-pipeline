## Build CodeBuild projects for Terraform Plan and Terraform Apply
module "codebuild_terraform_plan" {
  source                 = "./codebuild"
  pipeline_name          = var.pipeline_name
  codebuild_project_name = var.terraform_plan_codebuild_project_name
  codebuild_iam_role_arn = var.codebuild_iam_role_arn
  buildspec_name         = "foundation/buildspec_tf_plan.yml"
  default_tags           = var.default_tags
}

## Build CodeBuild projects for Terraform Plan and Terraform Apply
module "codebuild_terraform_apply" {
  source                 = "./codebuild"
  pipeline_name          = var.pipeline_name
  codebuild_project_name = var.terraform_apply_codebuild_project_name
  codebuild_iam_role_arn = var.codebuild_iam_role_arn
  buildspec_name         = "foundation/buildspec_tf_apply.yml"
  default_tags           = var.default_tags
}
