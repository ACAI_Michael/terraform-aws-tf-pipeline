output "terraform_plan_codebuild_project_name" {
  value = var.terraform_plan_codebuild_project_name
}

output "terraform_apply_codebuild_project_name" {
  value = var.terraform_apply_codebuild_project_name
}