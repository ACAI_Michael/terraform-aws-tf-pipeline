resource "aws_codebuild_project" "codebuild_project" {
  name          = var.codebuild_project_name
  description   = var.codebuild_project_description
  build_timeout = "5"
  service_role  = var.codebuild_iam_role_arn

  artifacts {
    type = "CODEPIPELINE"
  }

  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "aws/codebuild/standard:4.0"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
  }

  logs_config {
    cloudwatch_logs {
      group_name  = format("%s-loggroup", var.pipeline_name)
      stream_name = "log-stream"
    }

  }

  source {
    type      = "CODEPIPELINE"
    buildspec = var.buildspec_name
  }

  tags = var.default_tags
}