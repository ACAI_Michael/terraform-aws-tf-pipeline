variable "pipeline_name" {
  type = string
}

variable "codebuild_project_name" {
  type = string
}

variable "codebuild_project_description" {
  type    = string
  default = ""
}

variable "codebuild_iam_role_arn" {
  type = string
}

variable "buildspec_name" {
  type = string
}

variable "default_tags" {
  type    = map(string)
  default = {}
}
