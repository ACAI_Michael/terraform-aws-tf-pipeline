output "codebuild_arn" {
  value = aws_iam_role.codebuild.arn
}

output "codebuild_role_name" {
  value = var.codebuild_role_name
}

output "codecommit_repo_arn" {
  value = module.codecommit.codecommit_repo_arn
}
